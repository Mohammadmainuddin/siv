#!/usr/bin/env python
import datetime
import subprocess
import os
import pwd
import hashlib
import json
import sys
import textwrap
from datetime import datetime
import grp
import argparse
from grp import getgrgid

#creating the txt files 

files = "touch report1.txt report2.txt verify.txt "
os.system(files)



parser = argparse.ArgumentParser(
    description=textwrap.dedent('''Initialization --> siv.py -i -D 'dir' -V 'ver_file' -R 'rep_file' -H 'hash'
                                 ----------------------------------------------------------------------------
                                Verification  --> siv.py -v -D 'dir' -V 'ver_file' -R 'rep_file' '''))
args_group = parser.add_mutually_exclusive_group()
args_group.add_argument("-i", "--initialize", action="store_true")
args_group.add_argument("-v", "--verification", action="store_true")
parser.add_argument("-D", "--monitored_directory", type=str)
parser.add_argument("-V", "--verification_file", type=str)
parser.add_argument("-R", "--r_file", type=str)
parser.add_argument("-H", "--hash_function", type=str, default="SHA-1")

args = parser.parse_args()

mon = args.monitored_directory
verif = args.verification_file
reprt = args.r_file # report file generation
a = args.hash_function #algorithm funtion


def browsr():
    global filename1
    x = browse_file1()
    filename1=StringVar(window,value=x)
    file1Entry = Entry(window,bd=4,width=40,textvariable=file1Name).grid(row=2,column=4)
    
def browsr2():
    global filename2
    y = browse_file()
    filename2=StringVar(window,value=y)
    file2Entry = Entry(window,bd=4,width=40,textvariable=file1Name).grid(row=2,column=4)


def make_group(path): #returns group owner of the file or directory
  stat_info = os.stat(path)
  gid = stat_info.st_gid
  group = grp.getgrgid(gid)[0]
  return group

def hash_gen_md5(string):
    """MD5 calculation"""
    ans = (md5s(string))
    return(ans)
    print(ans)


def hash_gen_sha1(string):#SHA1 calculation
    
    sha1 = hashlib.sha1()
    sha1.update(string)
    sha1sum = sha1.hexdigest()
    return(sha1sum)

if args.initialize:

    print("Initialization mood has been activated\n")
    start = datetime.utcnow()
    #existance of monitoring directory 
    if os.path.isdir(mon) == 1 or os.path.exists(mon) == 1:
        print ("directory or file  already exists\n")


        if a=='MD-5' or a=='SHA-1': #hashing technique algorithm for both sha1 or md5
            print ("Hash is supported")

            p=0  #parsed directory  number
            y=0  #parsed file number existed


            dir1=[]
            dir1_dirs={}
            dir1_file={}
            dir1_hash={}

            #if verification file already exists or not
            if os.path.isfile(verif)==1 and os.path.isfile(reprt)==1:
                print ("Verification directory already existed\n")

                if (os.path.commonprefix([mon, verif]) == mon) or (os.path.commonprefix([mon, reprt]) == mon):
                    print("Both report file and verification file are in inside \n")
                    sys.exit()
                else:
                    print("Both report file and verification file are in outside\n")

            else:
                os.open(verif, os.O_CREAT, mode=0o777)
                os.open(reprt, os.O_CREAT, mode=0o777)

                print("Both report file and verification file are created\n")

                if (os.path.commonprefix([mon, verif]) == mon) or (os.path.commonprefix([mon, reprt]) == mon):
                    print("Both Verification file and Report file files are in outside\n")
                    sys.exit()
                else:
                    print("Both report file and verification file are in outside\n")

            #ask for overwrite  the  previous
            OverWrite = raw_input("Do you want to overwrite? please enter y/n: ")\

            if OverWrite == "n":
                sys.exit()
            elif OverWrite== "y":
                for sdir, dirtry, content in os.walk(mon):
                    for folders in dirtry:
                        p+=1
                        path = os.path.join(sdir,folders)
                        size1 = os.path.getsize(path)
                        users1 = pwd.getpwuid(os.stat(path).st_uid).pw_name
                        group = getgrgid(os.stat(path).st_gid).gr_name
                        time =datetime.fromtimestamp(os.stat(path).st_mtime).strftime('%c')
                        l_access = oct(os.stat(path).st_mode & 0o777)

                        dir1_dirs[path]={"size":size1, "users":users1, "group": group, "time": time, "access": l_access}


                    for file in content:
                        y+=1
                        fpath = os.path.join(sdir, file)
                        fsize1 = os.path.getsize(fpath)
                        fusers1 = pwd.getpwuid(os.stat(fpath).st_uid).pw_name
                        fgroup = getgrgid(os.stat(fpath).st_gid).gr_name
                        ftime =datetime.fromtimestamp(os.stat(fpath).st_mtime).strftime('%c')
                        fl_access = oct(os.stat(fpath).st_mode & 0o777)

                        #message compatibility  with SHA1

                        if  a=="SHA-1":
                            htype="sha1"
                            hash_type=hashlib.sha1()
                            with open(fpath, 'rb') as sfile:
                                buff=sfile.read()
                                hash_type.update(buff)
                                fed_message=hash_type.hexdigest()

                        #message compatibility  with MD5
                        else:
                            htype="MD-5"
                            hash_type=hashlib.md5()
                            with open(fpath, 'rb') as mdfile:
                                buff=mdfile.read()
                                hash_type.update(buff)
                                fed_message=hash_type.hexdigest()

                        dir1_file[fpath]={"size":fsize1, "users":fusers1,"group": fgroup, "time": ftime, "access":fl_access, "hash": fed_message}


                dir1.append(dir1_dirs)
                dir1_hash={"hash_type":htype}
                dir1.append(dir1_file)
                dir1.append(dir1_hash)
                json_str=json.dumps(dir1,indent=2, sort_keys=True)
                print('\n Verification file has been generated')

                #write to the ver file
                with open (verif, 'wb') as r_file:
                    r_file.write(json_str)

                print("\nreport file has been generated")
                #write  to r_file
                with open(reprt, 'wb') as r_file:
                    end= datetime.utcnow()
                    r_file.write("Initialization has completed \n\n Monitored directory = " + mon + "\nVerification file =" + verif + "\nNumber of total directories parsed =" + str(p) + "\nNumber of total files parsed = " + str(y) + "\n Total Time = " + str(end - start) + "\n")
            else:
                print("Invalid option\n")
                sys.exit()
        else:
            print("invalid hash\n")
            sys.exit()

    else:
        print("Monitoring directory has not been found")
        sys.exit()

elif args.verification:
    start = datetime.utcnow()
    print("Verifiation mood started\n")

    if os.path.isfile(verif) == 1:
        print("Verification File is  exists already\n")

        if (os.path.commonprefix([mon, verif]) == mon) or (os.path.commonprefix([mon, reprt]) == mon):
            print("Both report file and verification file are in inside\n")
            sys.exit()
        else:
            print("Both report file and verification file are in outside\n")
    else:
        print("Verification file has not been found")
        sys.exit()
    p=0 #dirs no
    q=0 #file no
    w=0 #warning num

    with open (verif) as inp_file:
        json_dec=json.load(inp_file)

    with open (reprt,"a") as r2_file:
        r2_file.write("\n Verification mood has  started\n")

    for every_file in json_dec[2]:
        htype = every_file[2]

    with open (reprt,"a") as r2_file:

        for sdir, dirtry, content in os.walk(mon):

            for folders in dirtry:
                p+=1
                path = os.path.join(sdir,folders)
                size1 = os.path.getsize(path)
                users1 = pwd.getpwuid(os.stat(path).st_uid).pw_name
                group = getgrgid(os.stat(path).st_gid).gr_name
                rcnt = datetime.fromtimestamp(os.stat(path).st_mtime).strftime('%c')
                l_access = oct(os.stat(path).st_mode & 0o777)

                print("Dirs" + path + '\n')

                if path in json_dec[0]:
                    if size1 != json_dec[0][path]['size']:
                        r2_file.write("\nWarning  at directory  " + path + " has not same size\n")
                        w += 1
                    if users1 != json_dec[0][path]['users']:
                        r_file.write("\nWarning  at directory  " + path + " has not same user\n")
                        w += 1
                    if group != json_dec[0][path]['group']:
                        r2_file.write("\nWarning  at directory  " + path + " has not same group\n")
                        w += 1
                    if rcnt != json_dec[0][path]['time']:
                        r2_file.write("\nWarning  at directory  " + path + " has not same modification date\n")
                        w += 1
                    if l_access != json_dec[0][path]['access']:
                        r2_file.write("\nWarning at directory"+ path +" has modified last access priviliges\n")
                        w += 1
                else:
                    r2_file.write("\nWarning at directory " + path + " has been added\n")
                    w += 1

        for all_dirs in json_dec[0]:

            if os.path.isdir(all_dirs)==0:
                r2_file.write("\n Warning at direcroty" + all_dirs +"has deleted\n")
                w+=1

        for sdir, dirtry, content in os.walk(mon):

            for folders in content:
                p+=1
                path = os.path.join(sdir,folders)
                size1 = os.path.getsize(path)
                users1 = pwd.getpwuid(os.stat(path).st_uid).pw_name
                group = getgrgid(os.stat(path).st_gid).gr_name
                rcnt = datetime.fromtimestamp(os.stat(path).st_mtime).strftime('%c')
                l_access = oct(os.stat(path).st_mode & 0o777)
                print("Files" + path +"\n")

                #sha1
                if  htype == "SHA-1":
                    hash_type=hashlib.sha1()
                    with open(path, "rb") as sfile:
                        buff=sfile.read()
                        hash_type.update(buff)
                        fed_message=hash_type.hexdigest()

                #md5
                else:
                    hash_type=hashlib.md5()
                    with open(path, "rb") as mdfile:
                        buff=mdfile.read()
                        hash_type.update(buff)
                        fed_message=hash_type.hexdigest()

                if path in json_dec[1]:
                    if size1 != json_dec[1][path]['size']:
                        r2_file.write("\nWarning  at directory  " +path + " has not same size\n")
                        w += 1
                    if users1 != json_dec[1][path]['users']:
                        r2_file.write("\nWarning  at directory  " + path + " has not same user\n")
                        w += 1
                    if group != json_dec[1][path]['group']:
                        r2_file.write("\nWarning  at directory  " + path + " has not same group\n")
                        w += 1
                    if rcnt != json_dec[1][path]['time']:
                        r2_file.write("\nWarning  at directory  " + path + " has not same modification date\n")
                        w += 1
                    if l_access != json_dec[1][path]['access']:
                        r2_file.write("\nWarning at directory"+ path +" has modified last access priviliges\n")
                        w += 1

                else:
                    r2_file.write("\nWarning at directory " + path + " has  added\n")
                    w += 1

        for all_files in json_dec[1]:
            if os.path.isdir(all_files)==0:
                r2_file.write("\n Warning at direcroty" + all_files +" has deleted\n")

##Finally writing to the rep file

                with open(reprt , "a") as r_file:
                    end = datetime.utcnow()
                    r_file.write("\nVerification mode completed \n\nMonitored directory = " + mon + "\nVerification file =" + verif + "\nNumber of directories parsed =" + str(p) + "\nNumber of files parsed = " + str(q) + "\nTotal Warnings = " + str(w) + "\nTime taken = " + str(end - start) + "\n")

                print("Report File has generated and completed ")

	
	
	

